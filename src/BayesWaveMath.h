/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

/* ********************************************************************************** */
/*                                                                                    */
/*                                    Math tools                                      */
/*                                                                                    */
/* ********************************************************************************** */

double gaussian_norm(double min, double max, double sigma);

void recursive_phase_evolution(double dre, double dim, double *cosPhase, double *sinPhase);

double network_nwip(int imin, int imax, double **a, double **b, double **invSn, int NI);
double network_snr(int imin, int imax, double **h, double **invpsd, int NI );
double detector_snr(int imin, int imax, double *g, double *invpsd, double eta);
double fourier_nwip(int imin, int imax, double *a, double *b, double *invSn);

/* ***********************************************************************************/
/*                   																					                       */
/*                                  Matrix Routines                                  */
/*											                                                             */
/* ***********************************************************************************/

void matrix_eigenstuff(double **matrix, double **evector, double *evalue, int N);
double matrix_jacobian(double **matrix, int N);
void matrix_multiply(double **matrix, double *vector, double *result, int N);
double dot_product(double *a, double *b, int N);


/* ********************************************************************************** */
/*																					                                          */
/*                                       RNGS                                         */
/*											                                                              */
/* ********************************************************************************** */

double gaussian_draw(gsl_rng *seed);
double uniform_draw(gsl_rng *seed);


/* ********************************************************************************** */
/*                                            */
/*                                 Fourier Routines                                   */
/*                                            */
/* ********************************************************************************** */

void fftw_wrapper(double *data, int N, int flag);
void tukey(double *data, double alpha, int N);
void tukey_scale(double *s1, double *s2, double alpha, int N);

